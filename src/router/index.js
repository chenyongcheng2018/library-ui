import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

export default new Router({
    routes: 
        [{
            path: '/',
            redirect: '/login'
        },
        {
            path: '/hello',
            component: resolve => require(['../components/library/hello.vue'], resolve)
        },
        {
            path: '/bdkh',
            component: resolve => require(['../components/library/bdkh.vue'], resolve)
        },
        {
            path: '/mybook',
            component: resolve => require(['../components/library/mybook.vue'], resolve)
        },
        {
            path: '/scanCode',
            component: resolve => require(['../components/library/scanCode.vue'], resolve)
        },
        {
            path: '/record',
            component: resolve => require(['../components/library/record.vue'], resolve)
        },
        {
            path: '/pay',
            component: resolve => require(['../components/library/pay.vue'], resolve)
        },
        {
            path: '/redirect',
            component: resolve => require(['../components/library/redirect.vue'], resolve)
        },
        {
            path:'*',
            redirect:'/bdkh'
        }
       
    ]
})
