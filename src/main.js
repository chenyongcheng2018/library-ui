import Vue from 'vue'
import App from './App'
import router from './router'
import axios from 'axios'
import VueAxios from 'vue-axios'
import utils from './utils'

import YDUI from 'vue-ydui';
import 'vue-ydui/dist/ydui.rem.css';
import htmlToPdf from '@/components/util/htmlToPdf';

Vue.prototype.$utils=utils 

Vue.use(YDUI);
Vue.use(VueAxios, axios)
Vue.use(htmlToPdf)

Vue.config.productionTip = false
axios.defaults.withCredentials = true;

new Vue({
    el: '#app',
    router,
    template: '<App/>',
    components: { App },
    render: h => h(App)
})

export { app, router }


